2020年1月9日
1.把道晟微服务 整套 复制一份 作为 智慧社区的 基础版本
2.生成自己的分支，确保基础版无误则上传分支
3.新建一个子模块，用来 写接下来的业务功能
  3.1lilin 负责 基础信息模块 增删改查 
        
  3.2玉超 负责 物业管理里的设备报修功能（提交新增订单 接口，修改订单接口  ，删除订单接口 ，查询订单接口 《订单基本信息 需要带图片》）

4.要求
  4.1 字段 要求完整   
  4.2 新增子系统 为 wuye
  4.3 新增该子系统业务对应的表明时 默认前面带  wy_   （居民 例如  wy_zhuhu）   
  4.4 建表 建字段 每个字段必须带有注释  。每个表必须带中文注释

  4.5 上传提交 忽略 编辑器的配置文件 ， class文件  切上传业务 时 注释下上传的内容